package cn.banyingli.banyinglilib.image;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import cn.banyingli.banyinglilib.R;
import cn.banyingli.banyinglilib.view.ImageLoadingView;
import okhttp3.OkHttpClient;

public class ImageLoaderUtils {
    private static ImageLoaderUtils instance = null;
    private static Picasso mPicasso;
    private static OkHttpClient okHttpClient;
    public static ImageLoaderUtils getInstance(Context context, OkHttpClient client) {
        if (null == instance) {
            instance = new ImageLoaderUtils();
            okHttpClient = client;
            mPicasso = new Picasso.Builder(context).downloader(new OkHttp3Downloader(okHttpClient)).build();
        }
        return instance;
    }

    /**
     * 加载图片（带进度回显）
     *
     * @param activity
     * @param imageView
     * @param url
     */
    public void loadImageWithProgress(final Activity activity, ImageView imageView, String url, OkHttp3DownloaderWithProgress.ProgressListener listener) {
        final ImageLoadingView loadingView = new ImageLoadingView(activity);
//        loadingView.loadCompleted(ImageLoadingView.ViewType.IMAGE);
        loadingView.setTargetView(imageView);
        OkHttp3DownloaderWithProgress.getPicasso(activity, okHttpClient, listener).load(url)
                .placeholder(R.drawable.onloading_square)
                .error(R.drawable.loaderror_square)
                .into(imageView);
    }

    /**
     * 加载图片（带进度回显）
     *
     * @param activity
     * @param imageView
     * @param url
     */
    public void loadImageWithProgress(final Activity activity, ImageView imageView, String url) {
        final ImageLoadingView loadingView = new ImageLoadingView(activity);
        loadingView.setTargetView(imageView);
        OkHttp3DownloaderWithProgress.getPicasso(activity, okHttpClient, new OkHttp3DownloaderWithProgress.ProgressListener() {
            @Override
            public void update(final long current, final long total) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(total != 0){
                            loadingView.setProgress(current / total); //0.0 ~ 1.0;
                        }
                        loadingView.loadCompleted(ImageLoadingView.ViewType.IMAGE);
                        if(current >= total){
                            loadingView.loadCompleted();
                        }
                    }
                });

            }
        }).load(url)
            .placeholder(R.drawable.onloading_square)
            .error(R.drawable.loaderror_square)
            .into(imageView);
    }


    /**
     * 加载图片
     *
     * @param ctx
     * @param imageView
     * @param url
     */
    public void loadImage(Context ctx, ImageView imageView, String url) {
        mPicasso.load(url)
                .placeholder(R.drawable.onloading_square)
                .error(R.drawable.loaderror_square)
                .into(imageView);
    }

    public void loadLocalImage(Context ctx, ImageView imageView, String url) {
        Picasso.with(ctx).load(url)
                .placeholder(R.drawable.onloading_square)
                .error(R.drawable.loaderror_square)
                .into(imageView);
    }

    /**
     * 加载图片
     *
     * @param ctx
     * @param imageView
     * @param resId
     */
    public void loadImage(Context ctx, ImageView imageView, int resId) {
        mPicasso.load(resId)
                .placeholder(R.drawable.onloading_square)
                .error(R.drawable.loaderror_square)
                .into(imageView);
    }

    /**
     * 加载图片（指定最大宽高）
     *
     * @param ctx
     * @param imageView
     * @param url
     */
    public void loadImageFit(Context ctx, ImageView imageView,  String url, int targetWidth, int targetHeight) {
        mPicasso.load(url)
                .transform(new PicassoFitTransform(ctx, targetWidth, targetHeight))
                .noFade()
                .placeholder(R.drawable.onloading)
                .error(R.drawable.loaderror)
                .into(imageView);
    }

    /**
     * 加载图片（指定最大宽高+圆角）
     *
     * @param ctx
     * @param imageView
     * @param url
     */
    public void loadImageFitRound(Context ctx, ImageView imageView,  String url, int targetWidth, int targetHeight, int roundingRadius) {
        mPicasso.load(url)
                .placeholder(R.drawable.onloading_square)
                .error(R.drawable.loaderror_square)
                .transform(new PicassoFitRoundTransform(ctx, targetWidth, targetHeight, roundingRadius))
                .noFade()
                .into(imageView);
    }

    /**
     * 加载圆形图片
     *
     * @param ctx
     * @param imageView
     * @param url
     */
    public void loadCircleImage(Context ctx, ImageView imageView, String url) {
        mPicasso.load(url)
                .placeholder(R.drawable.onloading_square)
                .error(R.drawable.loaderror_square)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .transform(new PicassoCircleTransform(ctx))
                .into(imageView);

    }

    /**
     * 加载圆角图片
     *
     * @param ctx
     * @param imageView
     * @param url
     * @param roundingRadius
     */
    public void loadRoundImage(Context ctx, ImageView imageView, String url, int roundingRadius) {
        mPicasso.load(url)
                .placeholder(R.drawable.onloading_square)
                .error(R.drawable.loaderror_square)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .transform(new PicassoRoundTransform(ctx, roundingRadius))
                .into(imageView);
    }
}
