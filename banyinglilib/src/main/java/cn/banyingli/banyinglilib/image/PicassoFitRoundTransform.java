package cn.banyingli.banyinglilib.image;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

import com.squareup.picasso.Transformation;

import cn.banyingli.banyinglilib.tools.DeviceTools;

/**
 * 指定最大宽高+圆角
 */
public class PicassoFitRoundTransform implements Transformation {

    private Context mContext;
    private int targetWidth;
    private int targetHeight;
    private int roundingRadius;

    public PicassoFitRoundTransform(Context context, int targetWidth, int targetHeight, int roundingRadius) {
        this.mContext = context;
        this.targetWidth = targetWidth;
        this.targetHeight = targetHeight;
        this.roundingRadius = roundingRadius;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        Bitmap retBitmap = null;
        if (source.getWidth() == 0 || source.getHeight() == 0) {
            retBitmap = source;
        }

        if (source.getWidth() > source.getHeight()) {//横向长图
            if (source.getHeight() < targetHeight && source.getWidth() <= targetWidth) {
                retBitmap =  source;
            } else {
                //如果图片大小大于等于设置的高度，则按照设置的高度比例来缩放
                double aspectRatio = (double) source.getWidth() / (double) source.getHeight();
                int width = (int) (targetHeight * aspectRatio);
                if (width > targetWidth) { //对横向长图的宽度 进行二次限制
                    width = targetWidth;
                    targetHeight = (int) (width / aspectRatio);// 根据二次限制的宽度，计算最终高度
                }
                if (width != 0 && targetHeight != 0) {
                    Bitmap result = Bitmap.createScaledBitmap(source, width, targetHeight, false);
                    if (result != source) {
                        // Same bitmap is returned if sizes are the same
                        source.recycle();
                    }
                    retBitmap =  result;
                } else {
                    retBitmap =  source;
                }
            }
        } else {//竖向长图
            //如果图片小于设置的宽度，则返回原图
            if (source.getWidth() < targetWidth && source.getHeight() <= targetHeight) {
                retBitmap =  source;
            } else {
                //如果图片大小大于等于设置的宽度，则按照设置的宽度比例来缩放
                double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                int height = (int) (targetWidth * aspectRatio);
                if (height > targetHeight) {//对横向长图的高度进行二次限制
                    height = targetHeight;
                    targetWidth = (int) (height / aspectRatio);//根据二次限制的高度，计算最终宽度
                }
                if (height != 0 && targetWidth != 0) {
                    Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, height, false);
                    if (result != source) {
                        // Same bitmap is returned if sizes are the same
                        source.recycle();
                    }
                    retBitmap =  result;
                } else {
                    retBitmap =  source;
                }
            }
        }

        int widthLight = retBitmap.getWidth();
        int heightLight = retBitmap.getHeight();
        int radius = DeviceTools.dp2px(mContext, roundingRadius); // 圆角半径

        Bitmap output = Bitmap.createBitmap(retBitmap.getWidth(), retBitmap.getHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(output);
        Paint paintColor = new Paint();
        paintColor.setFlags(Paint.ANTI_ALIAS_FLAG);

        RectF rectF = new RectF(new Rect(0, 0, widthLight, heightLight));

        canvas.drawRoundRect(rectF, radius, radius, paintColor);
//        canvas.drawRoundRect(rectF, widthLight / 5, heightLight / 5, paintColor);

        Paint paintImage = new Paint();
        paintImage.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));
        canvas.drawBitmap(retBitmap, 0, 0, paintImage);
        retBitmap.recycle();
        return output;
    }

    @Override
    public String key() {
        return "transformation" + " desiredWidth";
    }

}

