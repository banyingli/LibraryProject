package cn.banyingli.banyinglilib.tools;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class StringTools {

    /**
     * 移除字符串中开头和结尾的指定字符
     * @param str
     * @param removeStr
     * @return
     */
    public static String removeFirstLastChar(String str, String removeStr){
        if(null == str || 0 == str.trim().length()){
            return "";
        }
        String s = str;
        int size = removeStr.length();
        if(s.startsWith(removeStr)){
            s = s.substring(size);
        }
        if(s.endsWith(removeStr)){
            s = s.substring(0, s.length() - size);
        }

        return s;
    }

    /**
     * 处理null字符串，使其返回空串
     * @param str
     * @return
     */
    public static String removeNull(String str){
        if(null == str || 0 == str.trim().length() || "null".equals(str)){
            return "";
        }
        return str.trim();
    }

    /**
     * 判断字符串是否为空串
     *
     * @param str
     * @return
     */
    public static boolean isEmpty(String str) {
        if (null == str || 0 == str.trim().length()) {
            return true;
        }
        return false;
    }

    /*
     *数字不足位数左补0
     *
     * @param str
     * @param strLength
     */
    public static String addZeroForNumLeft(String str, int strLength) {
        int strLen = str.length();
        if (strLen < strLength) {
            while (strLen < strLength) {
                StringBuffer sb = new StringBuffer();
                sb.append("0").append(str);//左补0
                str = sb.toString();
                strLen = str.length();
            }
        }
        return str;
    }

    /*
     *数字不足位数右补0
     *
     * @param str
     * @param strLength
     */
    public static String addZeroForNumRight(String str, int strLength) {
        int strLen = str.length();
        if (strLen < strLength) {
            while (strLen < strLength) {
                StringBuffer sb = new StringBuffer();
                sb.append(str).append("0");//右补0
                str = sb.toString();
                strLen = str.length();
            }
        }
        return str;
    }

    /**
     * InputStream转字符串
     *
     * @param inputStream
     * @return
     */
    public static String inputStreamToString(InputStream inputStream) {
        try {
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) != -1) {
                result.write(buffer, 0, length);
            }
            String str = result.toString(StandardCharsets.UTF_8.name());
            return str;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 字符串转InputStream
     *
     * @param str
     * @return
     */
    public static InputStream stringToInputStream(String str) {
        InputStream is = new ByteArrayInputStream(str.getBytes());
        return is;
    }

    /**
     * 将字符串列表拼接成字符串
     * @param delimiter 拼接符
     * @param list
     * @return
     */
    public static String joinStringList(String delimiter, List<String> list) {
        if(null == list || list.size() == 0) return "";
        StringBuilder sb = new StringBuilder();
        int offset = list.size() - 1;
        for (int i = 0; i < offset; i++) {
            sb.append(list.get(i)).append(delimiter);
        }
        sb.append(list.get(offset));
        return sb.toString();
    }

    /**
     * 将字符串数组拼接成字符串
     * @param delimiter 拼接符
     * @param arr
     * @return
     */
    public static String joinStringArray(String delimiter, String[] arr) {
        StringBuilder sb = new StringBuilder();
        int offset = arr.length - 1;
        for (int i = 0; i < offset; i++) {
            sb.append(arr[i]).append(delimiter);
        }
        sb.append(arr[offset]);
        return sb.toString();
    }
}