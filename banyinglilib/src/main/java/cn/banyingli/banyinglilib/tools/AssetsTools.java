package cn.banyingli.banyinglilib.tools;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.InputStream;
import java.util.stream.Stream;

public class AssetsTools {
    static final String PRE_URL = "file:///android_asset/";

    /**
     * 从assets资源目录读取文件。
     *
     * @param ctx
     *            : 从该Context获取assets资源(也可直接使用Activity)
     * @param fileName
     *            : 资源assets下的文件名(包含目录).如: appview/1.xhtml
     * @return
     */
    static public InputStream getFileStream(Context ctx, String fileName) {
        InputStream is = null;
        AssetManager assetManager = ctx.getAssets();
        try {
            is = assetManager.open(fileName);
            // assetManager.close(); //不能关闭,否则程序接下来运行时出错
        } catch (Exception e) {
            e.printStackTrace();
        }
        return is;
    }

    /**
     * 从assets资源目录读取文件。
     *
     * @param ctx
     *            : 从该Context获取assets资源(也可直接使用Activity)
     * @param fileName
     *            : 资源assets下的文件名(包含目录).如: appview/1.xhtml
     * @return
     */
    static public String readAssetFile(Context ctx, String fileName) {

        String str = "";
        try {
            AssetManager assetManager = ctx.getAssets();
            InputStream is = assetManager.open(fileName);
            try {
                str = StringTools.inputStreamToString(is);
            } finally {
                is.close();
                // assetManager.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }


    /**
     * 使用webview加载本地assets目录下的html文件：
     * web2.loadUrl("file:///android_asset/index.html") ;
     * 如果按照file:///assets/来写，反而找不到
     *
     * @param filePath
     * @return
     */
    public static String getUrl(String filePath) {
        return PRE_URL + filePath;
    }
}
