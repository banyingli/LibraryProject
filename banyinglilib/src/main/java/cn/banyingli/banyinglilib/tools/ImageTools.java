package cn.banyingli.banyinglilib.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.os.Build;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageTools {
    public static Drawable getDrawable(Context context, int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getDrawable(id);
        }
        return context.getResources().getDrawable(id);
    }

    /**
     * 生成一个圆形内带文字的图片（用于头像）
     * @param bitmapColor   圆的颜色
     * @param backColor        图片背景颜色
     * @param txtColor           文字的颜色
     * @param text                  文字
     * @param circleWidth     图片宽度
     * @param circleHeight    图片高度
     * @return
     */
    public static Bitmap drawCircleTextImg(int bitmapColor, int backColor, int txtColor, String text, int circleWidth, int circleHeight){
        if(circleHeight <= 0){ //做判断，如果传入的高度为0，则默认为60
            circleHeight = 60;
        }
        if(circleWidth <= 0){ //做判断，如果传入的宽度为0，则默认为60
            circleWidth = 60;
        }
        if(text == null){ //如果传入的文字为空，则设置为空格
            text = " ";
        }
        Bitmap bitmap = Bitmap.createBitmap(circleWidth, circleHeight, Bitmap.Config.ARGB_8888);//设置生成的bitmap为高清
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(bitmapColor);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(backColor);
        int raduis = Math.min(bitmap.getWidth()/2, bitmap.getHeight()/2);
        canvas.drawCircle(bitmap.getWidth()/2, bitmap.getHeight()/2, raduis, paint);
        Paint txtPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        txtPaint.setTextSize(bitmap.getHeight()/3);
        txtPaint.setColor(txtColor);
        txtPaint.setStrokeWidth(10);
        txtPaint.setTextSize(bitmap.getHeight()/3);
        float txtWid = txtPaint.measureText(text);
        if(text.length() == 1){
            canvas.drawText(text,(bitmap.getWidth()-txtWid)/2, bitmap.getHeight()/2+bitmap.getHeight()/3/2-5, txtPaint);
        }else{
            canvas.drawText(text,(bitmap.getWidth()-txtWid)/text.length(), bitmap.getHeight()/2+bitmap.getHeight()/3/2, txtPaint);
        }
        canvas.save();
        canvas.restore();
        return bitmap;
    }

    /**
     * 压缩图片
     * @param imageFile
     * @param reqWidth
     * @param reqHeight
     * @param compressFormat
     * @param quality
     * @param destinationPath
     * @return
     * @throws IOException
     */
    static File compressImage(File imageFile, float reqWidth, float reqHeight, Bitmap.CompressFormat compressFormat, int quality, String destinationPath) throws IOException {
        FileOutputStream fileOutputStream = null;
        File file = new File(destinationPath).getParentFile();
        if (!file.exists()) {
            file.mkdirs();
        }
        try {
            fileOutputStream = new FileOutputStream(destinationPath);
            // write the compressed bitmap at the destination specified by destinationPath.
            decodeSampledBitmapFromFile(imageFile, reqWidth, reqHeight).compress(compressFormat, quality, fileOutputStream);
        } finally {
            if (fileOutputStream != null) {
                fileOutputStream.flush();
                fileOutputStream.close();
            }
        }

        return new File(destinationPath);
    }

    /**
     * 由文件转成bitmap
     * @param imageFile
     * @param reqWidth
     * @param reqHeight
     * @return
     * @throws IOException
     */
    static Bitmap decodeSampledBitmapFromFile(File imageFile, float reqWidth, float reqHeight) throws IOException {
        // First decode with inJustDecodeBounds=true to check dimensions

        Bitmap scaledBitmap = null, bmp = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        bmp = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        float imgRatio = (float) actualWidth / (float) actualHeight;
        float maxRatio = reqWidth / reqHeight;

        if (actualHeight > reqHeight || actualWidth > reqWidth) {
            //If Height is greater
            if (imgRatio < maxRatio) {
                imgRatio = reqHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) reqHeight;

            }  //If Width is greater
            else if (imgRatio > maxRatio) {
                imgRatio = reqWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) reqWidth;
            } else {
                actualHeight = (int) reqHeight;
                actualWidth = (int) reqWidth;
            }
        }

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2,
                middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
        bmp.recycle();
        ExifInterface exif;
        try {
            exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(),
                    scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return scaledBitmap;


    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            inSampleSize *= 2;
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

}
