package cn.banyingli.banyinglilib.view;

public interface IDatePickerListener {
    void onDateSelected(String dateStr);
    void onDateSelectCancel();
}
