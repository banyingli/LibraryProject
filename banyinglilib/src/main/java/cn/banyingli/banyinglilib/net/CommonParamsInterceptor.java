package cn.banyingli.banyinglilib.net;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * OKHttp3拦截器，用于添加共通参数
 */
class CommonParamsInterceptor implements Interceptor {
    private boolean showLog = true;

    public CommonParamsInterceptor(boolean showLog){
        this.showLog = showLog;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response = null;
        Request requestProcess = null ;
        if("GET".equals(request.method())){
            String url =  request.url().toString() + "&source=android";
            Request.Builder builder =  request.newBuilder() ;
            builder.get().url(url);
            requestProcess =  builder.build();
            response = chain.proceed(requestProcess);
        } else {
            FormBody.Builder builder = new FormBody.Builder() ;
            RequestBody requestBody =  request.body() ;
            if(requestBody instanceof FormBody){
                FormBody formBody = (FormBody)requestBody ;
                for (int i=0;i<formBody.size();i++){
                    builder.add(formBody.encodedName(i),formBody.encodedValue(i));
                }
                builder.add("source","android");
            }
            requestProcess =  request.newBuilder().url(request.url().toString()).post(builder.build()).build() ;
            response = chain.proceed(requestProcess);
        }
        return response;
    }
}

