package cn.banyingli.banyinglilib.net;

import com.orhanobut.logger.Logger;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * OKHttp3拦截器，用于打印出网络请求以及响应的信息
 */
class LoggingInterceptor implements Interceptor {
    private boolean showLog = true;

    public LoggingInterceptor(boolean showLog){
        this.showLog = showLog;
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {

        Request request = chain.request();
        long t1 = System.nanoTime();
        if(this.showLog){
//            Logger.i(String.format("Sending request %s on %s%n%s",
//                    request.url(), chain.connection(), request.headers()));
        }

        Response response = chain.proceed(request);
        long t2 = System.nanoTime();
//        if(this.showLog){
//            Logger.i(String.format("Received response for %s in %.1fms%n%s",
//                    response.request().url(), (t2 - t1) / 1e6d, response.headers()));
//        }

        return response;
    }
}

