package cn.banyingli.banyinglilib.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetWorkUtils {
    /**
     * 网络是否可用
     * @param context
     * @return
     */
    public static boolean isNetWorkAvailable(Context context){
        //获取联网管理器
        ConnectivityManager connectivityManager= (ConnectivityManager)context. getSystemService(Context.CONNECTIVITY_SERVICE);
        //网络信息
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo!=null){
            return true;
        }
        return false;
    }
}
