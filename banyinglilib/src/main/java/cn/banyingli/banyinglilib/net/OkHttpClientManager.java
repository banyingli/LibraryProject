package cn.banyingli.banyinglilib.net;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.internal.$Gson$Types;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.banyingli.banyinglilib.tools.AssetsTools;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class OkHttpClientManager {
    private static OkHttpClientManager mInstance;
    private OkHttpClient mOkHttpClient;
    private Handler mDelivery;
    private Gson mGson;
    private static NetConfig netConfig;
    private static Context context;
//    private final HashMap<String, List<Cookie>> cookieStore = new HashMap<>();
    private PersistentCookieStore cookieStore;
    private OkHttpClientManager() {
        cookieStore = new PersistentCookieStore(context);
        mOkHttpClient = new OkHttpClient.Builder()
                .connectTimeout(netConfig.getConnectTimeout(), TimeUnit.SECONDS)
                .readTimeout(netConfig.getReadTimeout(), TimeUnit.SECONDS)
                .addInterceptor(new LoggingInterceptor(netConfig.getShowLog()))// 添加日志打印拦截器
                .cookieJar(new CookieJarImpl(cookieStore)) // Cookie永久化存储管理
        .build();

        mDelivery = new Handler(Looper.getMainLooper());
        mGson = new Gson();
    }

    public OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }

    public static OkHttpClientManager getInstance(Context ctx, NetConfig config) {
        netConfig = config;
        if (mInstance == null) {
            synchronized (OkHttpClientManager.class) {
                if (mInstance == null) {
                    context = ctx;
                    mInstance = new OkHttpClientManager();
                }
            }
        }
        return mInstance;
    }

    public void clearCookies(){
        if(null != cookieStore){
            cookieStore.removeAll();
        }
    }

    public String getCookie(){
        if(null != cookieStore){
            return cookieStore.getCookie();
        }
        return "";
    }

    private static OkHttpClientManager getInstance() {
        if (mInstance == null) {
            synchronized (OkHttpClientManager.class) {
                if (mInstance == null) {
                    mInstance = new OkHttpClientManager();
                }
            }
        }
        return mInstance;
    }

    //*************对外公布的方法************

    /**
     * 清除cookie
     */
//    public void clearCookies(){
//        cookieStore.clear();
//    }

    //异步get请求
    public void getAsyn(String url, ResultCallback callback) {
        if(netConfig.isUseMock()){
            _getMockData(context, url, callback);
        }else{
            getInstance()._getAsyn(url, callback);
        }
    }

    //异步的post请求
    public void postAsyn(String url, final ResultCallback callback, Param... params) {
        if(netConfig.isUseMock()){
            _getMockData(context, url, callback);
        }else{
            getInstance()._postAsyn(url, callback, params);
        }
    }

    //异步的post请求
    public void postAsyn(String url, Map<String, String> params, final ResultCallback callback) {
        if(netConfig.isUseMock()){
            _getMockData(context, url, callback);
        }else{
            getInstance()._postAsyn(url, callback, params);
        }
    }

    //上传文件
    public void upload(String url, Map<String, Object> params, final ResultCallback callback) {
        getInstance()._upload(url, callback, params);
    }

    /**
     * 下载文件
     * @param url      下载连接
     * @param saveDir  储存下载文件的SDCard目录
     * @param listener 下载监听
     * @author https://www.jianshu.com/p/3b269082cbbb
     */
    public void download(final String url, final String saveDir, final String fileName, final OnDownloadListener listener) {
        Request request = new Request.Builder().addHeader("Accept-Encoding", "identity").url(url).build();

        mOkHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // 下载失败
                listener.onDownloadFailed(e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                InputStream is = null;
                byte[] buf = new byte[2048];
                int len = 0;
                FileOutputStream fos = null;
                // 储存下载文件的目录
                String savePath = isExistDir(saveDir);
                try {
                    is = response.body().byteStream();
                    String contentDisposition = response.header("Content-Disposition");

//                    long total = response.body().contentLength();
                    long total = getContentLengthFromDisposition(contentDisposition);
                    File file = new File(savePath, fileName);
                    fos = new FileOutputStream(file);
                    long sum = 0;
                    while ((len = is.read(buf)) != -1) {
                        fos.write(buf, 0, len);
                        sum += len;
                        int progress = (int) (sum * 1.0f / total * 100);
                        // 下载中
                        listener.onDownloading(progress);
                    }
                    fos.flush();
                    // 下载完成
                    listener.onDownloadSuccess(file.getPath());
                } catch (Exception e) {
                    e.printStackTrace();
                    listener.onDownloadFailed(e);
                } finally {
                    try {
                        if (is != null)
                            is.close();
                    } catch (IOException e) {
                    }
                    try {
                        if (fos != null)
                            fos.close();
                    } catch (IOException e) {
                    }
                }
            }
        });
    }

    //*************对外公布的方法 END************

    /**
     * 获取挡板数据（挡板数据放在assets/mock目录下）
     * @param context
     * @param url
     * @param callback
     */
    private void _getMockData(Context context, String url, ResultCallback callback){
        String mockUrl = url.replace(netConfig.getRootPath(), "");
        mockUrl = mockUrl.split("\\?")[0]; // 去掉有些URL后面的参数
        final String string = AssetsTools.readAssetFile(context, "mock" + mockUrl);
        if (callback.mType == String.class) {
            sendSuccessCallBack(callback, string);
        } else {
            Object o = mGson.fromJson(string, callback.mType);
            sendSuccessCallBack(callback, o);
        }
    }

    /**
     * 异步的get请求
     *
     * @param url
     * @param callback
     */
    private void _getAsyn(String url, final ResultCallback callback) {
        final Request request = new Request.Builder()
                .url(url)
                .build();
        deliveryResult(callback, request);
    }

    /**
     * 异步的post请求
     *
     * @param url
     * @param callback
     * @param params
     */
    private void _postAsyn(String url, final ResultCallback callback, Param... params) {
        Request request = buildPostRequest(url, params);
        deliveryResult(callback, request);
    }

    /**
     * 异步的post请求
     *
     * @param url
     * @param callback
     * @param params
     */
    private void _postAsyn(String url, final ResultCallback callback, Map<String, String> params) {
        Param[] paramsArr = map2Params(params);
        Request request = buildPostRequest(url, paramsArr);
        deliveryResult(callback, request);
    }

    public void postFile(String url, final ProgressListener listener, final ResultCallback callback, File...files){

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        //第一个参数要与Servlet中的一致
//        String fileName = files[0].getName();
        String fileName = "";
        try{
            fileName = URLEncoder.encode(files[0].getName(), "UTF-8");
        }catch (Exception e){
            e.printStackTrace();
            fileName = "file";
        }
        builder.addFormDataPart("file",fileName, RequestBody.create(MediaType.parse("application/octet-stream; charset=utf-8"),files[0]));
        MultipartBody multipartBody = builder.build();
        Request request  = new Request.Builder().url(url).post(new ProgressRequestBody(multipartBody,listener)).build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                sendFailCallback(callback, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    final String string = response.body().string();
                    if (callback.mType == String.class) {
                        sendSuccessCallBack(callback, string);
                    } else {
                        Object o = mGson.fromJson(string, callback.mType);
                        sendSuccessCallBack(callback, o);
                    }

                } catch (IOException e) {
                    sendFailCallback(callback, e);
                } catch (com.google.gson.JsonParseException e)//Json解析的错误
                {
                    sendFailCallback(callback, e);
                }
            }
        });
    }

    /**
     * 上传文件
     *
     * @param url
     * @param callback
     * @param params
     */
    private void _upload(String url, final ResultCallback callback, Map<String, Object> params) {
        //创建OkHttpClient请求对象
        //创建MultipartBody.Builder
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            Object value = entry.getValue();
            if (value instanceof File) {
                File file = (File) value;
                //创建RequestBody 封装file参数
                String fileName = "";
                try{
                    fileName = URLEncoder.encode(file.getName(), "UTF-8");
                }catch (Exception e){
                    e.printStackTrace();
                    fileName = "file";
                }
                builder.addFormDataPart(entry.getKey(), fileName, RequestBody.create(MediaType.parse("application/octet-stream"), file));
            } else {
                builder.addFormDataPart(entry.getKey(), value.toString());
            }
        }
        //创建RequestBody 设置类型等
        RequestBody requestBody = builder.build();
        //创建Request
        Request request = new Request.Builder().url(url).post(requestBody).build();
        deliveryResult(callback, request);
    }




    private Param[] map2Params(Map<String, String> params) {
        if (params == null) return new Param[0];
        int size = params.size();
        Param[] res = new Param[size];
        Set<Map.Entry<String, String>> entries = params.entrySet();
        int i = 0;
        for (Map.Entry<String, String> entry : entries) {
            res[i++] = new Param(entry.getKey(), entry.getValue());
        }
        return res;
    }

    /**
     * 判断下载目录是否存在
     * @param saveDir
     * @return
     * @throws IOException
     */
    public static String isExistDir(String saveDir) throws IOException {
        // 下载位置
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {

            File downloadFile = new File(saveDir);
            if (!downloadFile.mkdirs()) {
                downloadFile.createNewFile();
            }
            String savePath = downloadFile.getAbsolutePath();
            Log.e("savePath", savePath);
            return savePath;
        }
        return null;
    }

    /**
     * 从下载连接中解析出文件名
     * @param url
     * @return
     */
    private static String getNameFromUrl(String url) {
        return url.substring(url.lastIndexOf("/") + 1);
    }

    /**
     * 下载监听
     */
    public interface OnDownloadListener {
        /**
         * 下载成功
         */
        void onDownloadSuccess(String path);

        /**
         * 下载进度
         */
        void onDownloading(int progress);

        /**
         * 下载失败
         */
        void onDownloadFailed(Exception e);
    }

    private void deliveryResult(final ResultCallback callback, Request request) {
        mOkHttpClient.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                sendFailCallback(callback, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    final String string = response.body().string();
                    if (callback.mType == String.class) {
                        sendSuccessCallBack(callback, string);
                    } else {
                        Object o = mGson.fromJson(string, callback.mType);
                        sendSuccessCallBack(callback, o);
                    }

                } catch (IOException e) {
                    sendFailCallback(callback, e);
                } catch (com.google.gson.JsonParseException e)//Json解析的错误
                {
                    sendFailCallback(callback, e);
                }
            }
        });
    }

    private void sendFailCallback(final ResultCallback callback, final Exception e) {
        mDelivery.post(new Runnable() {
            @Override
            public void run() {
                if (callback != null) {
                    callback.onFailure(e);
                }
            }
        });
    }

    private void sendSuccessCallBack(final ResultCallback callback, final Object obj) {
        mDelivery.post(new Runnable() {
            @Override
            public void run() {
                if (callback != null) {
                    callback.onSuccess(obj);
                }
            }
        });
    }

    private Request buildPostRequest(String url, Param[] params) {
        if (params == null) {
            params = new Param[0];
        }
        FormBody.Builder formBodyBuilder = new FormBody.Builder();
        for (Param param : params) {
            formBodyBuilder.add(param.key, param.value);
        }
        RequestBody requestBody = formBodyBuilder.build();
        return new Request.Builder()
                .url(url)
                .header("Referer", "")
                .post(requestBody)
                .build();
    }

    public static abstract class ResultCallback<T> {
        public Type mType;

        // ResultCallback(Function0<Unit> function)
        public ResultCallback() {
            mType = getSuperclassTypeParameter(getClass());
        }

        static Type getSuperclassTypeParameter(Class<?> subclass) {
            Type superclass = subclass.getGenericSuperclass();
            if (superclass instanceof Class) {
                throw new RuntimeException("Missing type parameter.");
            }
            ParameterizedType parameterized = (ParameterizedType) superclass;
            return $Gson$Types.canonicalize(parameterized.getActualTypeArguments()[0]);
        }

        /**
         * 请求成功回调
         *
         * @param response
         */
        public abstract void onSuccess(T response);

        /**
         * 请求失败回调
         *
         * @param e
         */
        public abstract void onFailure(Exception e);
    }

    /**
     * post请求参数类
     */
    public static class Param {
        public Param() {
        }

        public Param(String key, String value) {
            this.key = key;
            this.value = value;
        }

        String key;
        String value;
    }

    private long getContentLengthFromDisposition(String contentDisposition){
        long length = 1;
        String regEx="[^0-9]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(contentDisposition);
        String lenStr = m.replaceAll("").trim();
        try{
            length = Long.parseLong(lenStr);
        }catch (Exception e){
            e.printStackTrace();
        }
        return length;
    }

}