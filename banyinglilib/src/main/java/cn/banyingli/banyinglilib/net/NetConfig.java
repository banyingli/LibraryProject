package cn.banyingli.banyinglilib.net;

public class NetConfig {
    private String rootPath = "";
    private int connectTimeout = 30;
    private int readTimeout = 30;
    private int writeTimeout = 30;
    private boolean showLog = true;
    private boolean useMock = false;// 使用挡板数据

    public final int getConnectTimeout() {
        return this.connectTimeout;
    }

    public final void setConnectTimeout(int var1) {
        this.connectTimeout = var1;
    }

    public final int getReadTimeout() {
        return this.readTimeout;
    }

    public final void setReadTimeout(int var1) {
        this.readTimeout = var1;
    }

    public final boolean getShowLog() {
        return this.showLog;
    }

    public final void setShowLog(boolean var1) {
        this.showLog = var1;
    }

    public int getWriteTimeout() {
        return writeTimeout;
    }

    public void setWriteTimeout(int writeTimeout) {
        this.writeTimeout = writeTimeout;
    }

    public boolean isUseMock() {
        return useMock;
    }

    public void setUseMock(boolean useMock) {
        this.useMock = useMock;
    }

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }
}
